+++
title = "Book Review Recommender System"

[extra]
link = ""
technologies = ["machine learning", "recommender system"]
+++
• Trained models to make predictions related to book categories and reader preferences

• Determined threshold that gives maximum accuracy and used Jaccard function to calculate similarity 

• Cleaned up raw data and applied TF-IDF vectorization and logistic regression to predict the accuracy
