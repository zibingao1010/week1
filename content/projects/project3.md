+++
title = "Wine Quality and Price Analysis"

[extra]
link = ""
technologies = ["group work", "model training"]
+++
• Worked in five-person team to analyze the different factors that affect wine quality

• Created scatterplots, histograms, and heat maps to enhance accuracy based on three datasets of wine information 

• Used correlation analysis and XGBoost model to predict wine prices and validate the hypothesis

